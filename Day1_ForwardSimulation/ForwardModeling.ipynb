{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Solving the wave equation</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">Time-domain spectral-element simulations</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "# Import modules and setup paths.\n",
    "import os\n",
    "import h5py\n",
    "import pyasdf\n",
    "import salvus_seismo\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from obspy.clients.fdsn import Client\n",
    "\n",
    "# Path to Paraview\n",
    "PARAVIEW_BIN = '/opt/paraview/bin/paraview'\n",
    "# Uncomment for OSX\n",
    "# PARAVIEW_BIN = '/Applications/ParaView-5.6.0.app/Contents/MacOS/paraview'\n",
    "\n",
    "all_rec_names = ['rec1', 'rec2', 'rec3', 'rec4', 'rec5']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hello everyone, and welcome to a short tutorial on forward modeling, in which we will use the spectral-element method (SEM) for time-domain simulations of the elastic wave equation. \n",
    "The tutorial is meant to get you familiar with some of the most important options of the simulation software Salvus, and quickly introduce some of the advanced workflow tools we are currently developing for global seismology."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Lamb's problem\n",
    "\n",
    "Our first stop is the solution of Lamb's problem in two dimensions. This involves the simulation of the elastic wave equation in the presence of a free surface boundary. Our source will be placed 500m below the surface, where it will excite motion by applying a force in the vertical (y) direction. We'll place several receivers closer to the surface (200m below), so we can capture both the direct arriving wave, as well as its reflection off the boundary.\n",
    "\n",
    "At this stage we won't worry about designing a mesh from scratch, but use a predefined rectilinear mesh instead. Go ahead and open the mesh file `Quad_IsotropicElastic2D_Nodal_44x22.e` in `Paraview`. You can select and download it from the `Files` tab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN Quad_IsotropicElastic2D_Nodal_44x22.e"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take some time here to explore `Paraview`, and to get comfortable with some of its features (we'll be using it a lot). To load the mesh file into memory, click the `Apply` button in `Paraview`. You should then see a rectangle against a gradient background -- that's our simulation domain! To make things a bit more interesting, click on drop-down menu `Surface`, and select `Surface With Edges`. This turns on the element connectivity. You should now see the element superimposed onto the domain, and there should be 22 elements in the vertical direction, and 44 in the horizontal. You can also visualize the material parameters by clicking the checkboxes in the `Variables` panel on the left hand side of the screen. When you do this, click `Apply` again to load these into memory. Now, in the parameter selection drop down menu (currently set to `vtkBlockColors`) you should see a listing of the defined material parameters. Click around and check them out.\n",
    "\n",
    "**Important! Salvus works exclusively in SI units (meters, g/m^3). Of course, it will use whatever parameters you pass on to it... just don't expect physical results if you mix units.**\n",
    "\n",
    "Our mesh is looking a little boring at the moment -- some wave propagation would do it well. But before we get there, let's see what we're aiming for."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing seismograms\n",
    "\n",
    "Before we make our own seismograms, we'll load and explore the analytic solutions. The solutions generated from our tests are located in this example directory, in the file `force_verticale.h5`. This is an ASDF file containing the waveforms themselves along with relevant meta information. You can open and plot the data as below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(1, 2, sharex='col', sharey='row', figsize=(20,5))\n",
    "with pyasdf.ASDFDataSet(\"force_verticale.h5\", mode=\"r\") as dataset:\n",
    "    axes[0].plot(dataset.waveforms.XX_REC1.displacement.select(component='Z')[0].data)\n",
    "    axes[1].plot(dataset.waveforms.XX_REC1.displacement.select(component='E')[0].data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also easily loop through all 5 seismograms in that file and plot them. There's a simple function to do this below. We'll re-use it later on to compare our own solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_hdf5_seismograms(asdf_file, axes):\n",
    "    # Salvus labels the Cartesian axis as 'X' and 'Y', the reference solution uses 'Z' and 'E'\n",
    "    par_map = ['Z', 'E']\n",
    "    new_par_map = ['X', 'Y']\n",
    "    for i, waveform in enumerate(asdf_file.waveforms):\n",
    "        for j in range(len(par_map)):\n",
    "            try:\n",
    "                axes[j,i].plot(waveform.displacement.select(component=par_map[j])[0].data)\n",
    "            except:\n",
    "                axes[j,i].plot(waveform.displacement.select(component=new_par_map[j])[0].data)\n",
    "\n",
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20, 5))\n",
    "with pyasdf.ASDFDataSet(\"force_verticale.h5\", mode=\"r\") as dataset:\n",
    "    plot_hdf5_seismograms(dataset, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running the solver\n",
    "\n",
    "Now let's get to the fun stuff -- actually running some simulations. We could give it a try immediately, and just run salvus from the command line..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!salvus $1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...but of course that won't work, as we haven't passed any parameters yet. There are a rich variety of parameters and options that Salvus can use, and we'll get to most of them in the course of the tutorials. Our goal here however is to just set up and run a simple simulation -- nothing fancy. Salvus is meant to be user friendly, so it should instruct you on how to fail forward. With that in mind, we'll shortly send you out into the wild after a couple quick hints.\n",
    "\n",
    "Currently, the output is complaining that it is missing `domain` and `physics` root objects. Let's make a TOML file with these. \n",
    "\n",
    "**Hint:** The `%%writefile` directive is a special Jupyter notebook directive which writes the content of the cell to the given file once executed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "\n",
    "[physics]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now just pass this file to salvus."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great, another error message. At least this one is different though -- it's telling us that we also need to provide a few other things.\n",
    "\n",
    "You can mostly work your way through Salvus by putting in some more options and checking what messages it gives. For the sake of time though, we'll pass a minimal set of parameters that work. Try this below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    end-time-in-seconds = 0.52"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Attaching sources\n",
    "\n",
    "Now we know how to visualize receivers, and run a simple simulation with a mesh. Of course, something is seriously lacking! We didn't actually attach any external sources to the model... we were just crunching a lot of 0s. The next step is to add some simple seismic sources. For this example we'll proceed with specifying a single source."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    end-time-in-seconds = 0.52\n",
    "    source-toml-filename = \"source.toml\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sources are specified in separate TOML files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile source.toml\n",
    "\n",
    "[[source]]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we can again follow the path outlined by the error messages.\n",
    "\n",
    "As before, we could stumble our way through the options and inform ourselves through the various polite error messages, but we'll just write down and run the minimum working set below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile source.toml\n",
    "\n",
    "[[source]]\n",
    "name = \"source1\"\n",
    "location = [0.0, 1500.0]\n",
    "temporal_type = \"ricker\"\n",
    "spatial_type = \"vector\"\n",
    "center_frequency = 14.5\n",
    "scale = [1e9, 1e9]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ok, and there we go. It looks like everything has completed successfully, with no complaints. Now we have (apparently) injected some energy and got a meaningful result. Now we just need to see what it is we have done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing the solution\n",
    "\n",
    "We'll eventually get to measuring the data at receivers, but it is often very useful (and just darn cool) to see a movie of what we're doing. It also allows us to reason about how the choices we've made above affect the simulation results. Fortunately, this is quite easy -- it just requires a few more options. In keeping with our anarchic spirit, we'll give the hint that a new `[output.volume-data]` must be added."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    end-time-in-seconds = 0.52\n",
    "    source-toml-filename = \"source.toml\"\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 10\n",
    "    filename = \"wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The movie is now created -- if you look in this example directory you should see a `wavefield.h5`, or whatever you decided to name it (the .h5 suffix tells us that this, like the receivers, is also an hdf5 file). Additionally the corresponding XDMF file should be there. Open it with paraview."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN wavefield_ELASTIC.xdmf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pretty neat. Spend a bit of time playing with the different options we've specified so far, and see how this affects the resulting movie. Try and make the simulation explode -- it shouldn't be too hard. Try setting the start time and the end-time to different values, and changing the frequency content of the source, to see how the solution changes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Receivers\n",
    "\n",
    "Of course, we didn't come here to watch pretty movies -- our goal is to recreate that analytical test! We're almost there, we just need to add some receivers. This time, the options will be spelt out for you. You need to add:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    end-time-in-seconds = 0.52\n",
    "    source-toml-filename = \"source.toml\"\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 10\n",
    "    filename = \"wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    \n",
    "[output.point-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 1\n",
    "    filename = \"receivers.h5\"\n",
    "    receiver-toml-filename = \"receivers.toml\"\n",
    "    format = \"ASDF\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Receivers are again in a separate file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile receivers.toml\n",
    "\n",
    "[[receiver]]\n",
    "    network = \"XX\"\n",
    "    station = \"REC1\"\n",
    "    salvus_coordinates = [10, 1800]\n",
    "    medium = \"solid\"\n",
    "    \n",
    "[[receiver]]\n",
    "    network = \"XX\"\n",
    "    station = \"REC2\"\n",
    "    salvus_coordinates = [110, 1800]\n",
    "    medium = \"solid\"\n",
    "    \n",
    "[[receiver]]\n",
    "    network = \"XX\"\n",
    "    station = \"REC3\"\n",
    "    salvus_coordinates = [210, 1800]\n",
    "    medium = \"solid\"\n",
    "    \n",
    "[[receiver]]\n",
    "    network = \"XX\"\n",
    "    station = \"REC4\"\n",
    "    salvus_coordinates = [310, 1800]\n",
    "    medium = \"solid\"\n",
    "    \n",
    "[[receiver]]\n",
    "    network = \"XX\"\n",
    "    station = \"REC5\"\n",
    "    salvus_coordinates = [410, 1800]\n",
    "    medium = \"solid\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, there is an ASDF file in the currenty directory called receivers.h5. This has the same format as the file we've already looked at, so we can just use the same script to look at it again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "\n",
    "with pyasdf.ASDFDataSet('receivers.h5', mode=\"r\") as salvus_rec_file:\n",
    "    plot_hdf5_seismograms(salvus_rec_file, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definitely look like seismograms! How do these compare to the analytic ones we're testing against? Just call that function twice, once with the new file handle, and once with the old one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "with pyasdf.ASDFDataSet('receivers.h5', mode=\"r\") as salvus, \\\n",
    "        pyasdf.ASDFDataSet('force_verticale.h5', mode=\"r\") as analytic:\n",
    "    plot_hdf5_seismograms(salvus, axes)\n",
    "    plot_hdf5_seismograms(analytic, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hmm, not so ideal. It's up to you now to make them match. Here are the parameters you need. Time step: 1e-3 Start time: -8e-2 End time: 5.2e-1 Ricker central frequency: 14.5 Source location: (0,1500) Source direction: (0,-1e10) Good luck!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -8e-2\n",
    "    end-time-in-seconds = 0.52\n",
    "    time-step-in-seconds = 1e-3\n",
    "    source-toml-filename = \"source.toml\"\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 10\n",
    "    filename = \"wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    \n",
    "[output.point-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 1\n",
    "    filename = \"receivers.h5\"\n",
    "    receiver-toml-filename = \"receivers.toml\"\n",
    "    format = \"ASDF\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile source.toml\n",
    "\n",
    "[[source]]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "with pyasdf.ASDFDataSet('receivers.h5', mode=\"r\") as salvus, \\\n",
    "        pyasdf.ASDFDataSet('force_verticale.h5', mode=\"r\") as analytic:\n",
    "    plot_hdf5_seismograms(salvus, axes)\n",
    "    plot_hdf5_seismograms(analytic, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So close. There is sometimes discrepancies at late times however. These are from the reflections from the mesh edges. It helps to turn on the absorbing boundaries at all edges except the free surface. This is located at the top, labelled by side set y1. We can turn on absorbing boundaries on the other edges by specifying absorbing boundaries in the physics group."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"Quad_IsotropicElastic2D_Nodal_44x22.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -8e-2\n",
    "    end-time-in-seconds = 0.52\n",
    "    time-step-in-seconds = 1e-3\n",
    "    source-toml-filename = \"source.toml\"\n",
    "\n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 10\n",
    "    filename = \"wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    \n",
    "[output.point-data]\n",
    "    fields = [\"u_ELASTIC\"]\n",
    "    sampling-interval-in-time-steps = 1\n",
    "    filename = \"receivers.h5\"\n",
    "    receiver-toml-filename = \"receivers.toml\"\n",
    "    format = \"ASDF\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(2, 5, sharex='col', sharey='row', figsize=(20,5))\n",
    "with pyasdf.ASDFDataSet('receivers.h5', mode=\"r\") as salvus, \\\n",
    "        pyasdf.ASDFDataSet('force_verticale.h5', mode=\"r\") as analytic:\n",
    "    plot_hdf5_seismograms(salvus, axes)\n",
    "    plot_hdf5_seismograms(analytic, axes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Nice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global seismology\n",
    "\n",
    "Most realistic problems involve larger domains, more complicated meshes, and additional physics. This requires simulation tools that run efficiently in both 2 and 3 dimensions, and which scale on the largest HPC clusters.\n",
    "\n",
    "What follows is a quick example of some functionality we're working on to make global scale inversions more tractable. We'll also use the salvus_seismo subpackage to make adding source and receiers a breeze. While we're using a 2D example here so it can be run locally, the steps are the same for running 3D global simulations. First, let's generate a mesh for an Earth model suitable for a 100 second simulation. Below is an example of a parameter file that we can pass to salvus_mesher. This model is based on PREM, but with all the thin layers removed. This is in order to keep the timestep reasonable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat velocity_model.bm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's generate the mesh using the commands below..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!python -m salvus_mesher.interface Circular2D --basic.period=100 \\\n",
    "--chunk2D.max_colatitude=180 --basic.model 'velocity_model.bm' \\\n",
    "--output_filename '2D_Globe.e' --overwrite"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and view the mesh with Paraview."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN ./2D_Globe.e"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's make up a simple moment tensor solution to inject into the mesh. We'll use salvus_seismo to make sure that it is rotated into the correct coordinate frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "src = salvus_seismo.Source(longitude=0.0, depth_in_m=1,\n",
    "                           m_rr=1e20, m_rp=0, m_pp=1e20,\n",
    "                           center_frequency=0.004)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now we'll query IRIS for all the stations that belong to the 'IU' network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = Client(\"IRIS\")\n",
    "recs = salvus_seismo.Receiver.parse(c.get_stations(network=\"IU\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll use salvus_seismo to generate the command line call for us. No need to remember and write down all those pesky command line options each time. Let's use it to generate a `configure` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Again a short simulation for speed reasons.\n",
    "config = salvus_seismo.Config(\n",
    "    mesh_file=\"./2D_Globe.e\",\n",
    "    end_time=2700,\n",
    "    salvus_call='mpirun -n 1 salvus',\n",
    "    polynomial_order=4,\n",
    "    verbose=False,\n",
    "    dimensions=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we'll use all this stuff we generated to write all our parameters to a set of files which Salvus can run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ensure a clean directory.\n",
    "!rm -rf webservice_example/\n",
    "salvus_seismo.generate_cli_call(\n",
    "    source=src, receivers=recs, config=config,\n",
    "    output_folder=\"webservice_example\",\n",
    "    exodus_file=\"./2D_Globe.e\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note what we've done here. Along with our sources and receivers, salvus_seismo takes a mesh file -- '2D_Globe.e' -- as an argument. This serves to assist with the placement of sources and receivers (project them onto the surface of the mesh -- very handy with topography). All the relevant data should now be in the `webservice_example` sub-directory. Let's look at some files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat webservice_example/run_salvus.sh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat webservice_example/receivers.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's run the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Run it.\n",
    "!sh ./webservice_example/run_salvus.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now, let's look at some receivers..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "receivers = pyasdf.ASDFDataSet('receiver.h5')\n",
    "for i, waveform in enumerate(receivers.waveforms):\n",
    "    waveform.displacement.plot()\n",
    "    input()"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

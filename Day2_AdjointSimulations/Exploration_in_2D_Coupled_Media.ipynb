{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Adjoint simulations and gradients</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">Seismic exploration in 2D coupled media</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "# Import modules and setup paths.\n",
    "import os\n",
    "import h5py\n",
    "import pyasdf\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pathlib\n",
    "import toml\n",
    "\n",
    "from salvus_mesher.structured_grid_2D import StructuredGrid2D\n",
    "from salvus_mesher.skeleton import Skeleton\n",
    "\n",
    "# Path to Paraview\n",
    "PARAVIEW_BIN = '/opt/paraview/bin/paraview'\n",
    "# Uncomment for OSX\n",
    "# PARAVIEW_BIN = '/Applications/ParaView-5.5.0.app/Contents/MacOS/paraview'\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, we will simulate waves propagating through coupled solid-fluid media with a sea bottom topography. By the end of the tutorial you should be able to generate a mesh, attach sources and receivers, generate simple movies, run simulations in coupled solid-fluid media and compute sensitivity kernels. \n",
    "\n",
    "Let's get started!\n",
    "\n",
    "### Generating the mesh\n",
    "\n",
    "The first step is to generate an appropriate mesh for the problem. In general, meshes can be generated from commercial meshing packages, such as Trelis, or with the Salvus ecosystem itself, using `salvus_mesher`. For this tutorial, we'll generate a simple layered model with sea bottom topography.\n",
    "\n",
    "In this example, we'll mock a simple oceanic seismic survey with constant material parameters in both media."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# material parameters\n",
    "vp_solid  = 2500.0        # p-wave velocity (m/s) in the solid\n",
    "vs_solid  = 1500.0        # s-wave velocity (m/s) of the solid\n",
    "rho_solid = 2600.0        # density (kg/m^3) of the solid\n",
    "\n",
    "vp_fluid  = 1500.0        # p-wave velocity (m/s) of the fluid\n",
    "rho_fluid = 1000.0        # density (kg/m^3) of the fluid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to define the computational domain. Let's set the horizontal distance to 10 km. Since we're looking for something that can be run on a laptop, we will keep the mesh relatively small. With those dimensions, a mesh accurate to 4 Hz fulfills this criteria well. Of course, if you've got some beefier hardware, feel free to increase the frequency. As a rule of thumb, we roughly need two fourth-order spectral elements per wavelength to accurately solve the wave equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "max_x = 10000.          # domain width in m\n",
    "fmax = 4.               # maximum frequency in Hz\n",
    "elements_per_wavelength = 2.\n",
    "\n",
    "# compute edgelengths\n",
    "hmax = vs_solid / fmax / elements_per_wavelength"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next part is a bit technical and defines the sea bottom topography. \n",
    "We are just making up some smooth topography by concatenating trigonometric functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# some artificial topography model - h(x)\n",
    "nelem_x = int(np.ceil(max_x / hmax))\n",
    "h0 = 1.6\n",
    "h1 = .5\n",
    "h2 = -.1\n",
    "h3 = .15\n",
    "\n",
    "x = np.linspace(0., max_x, nelem_x + 1)\n",
    "norm_x = x / max_x * 2 * np.pi\n",
    "h = h0 - h1 * np.cos(norm_x) - h2 * np.cos(2 * norm_x) - h3 * np.sin(3 * norm_x)\n",
    "h = h * max_x / 2 / np.pi\n",
    "\n",
    "# waterlevel\n",
    "wl = 2.6\n",
    "wl *= max_x / 2 / np.pi\n",
    "\n",
    "# waterheight\n",
    "wh = np.maximum(0, wl - h)\n",
    "\n",
    "# number of vertical elements needed for each element in horizontal direction\n",
    "nelem_y = np.ceil(h / hmax).astype('int')\n",
    "nelem_y_w = np.maximum(1, np.ceil((wl - h) / hmax).astype('int'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we generate the two parts of the mesh corresponding to the solid and the fluid region. Both are based on a structured 2D grid with a predefined number of elements in x and y direction. Note that the number of elements in the vertical direction varies. The mesher will automatically refine the mesh when needed to ensure that we obtain a conforming quadrilateral mesh. This is very important for the spectral element method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create box mesh with refinements\n",
    "solid_grid = StructuredGrid2D.rectangle_vertical_refine(\n",
    "    nelem_x, nelem_y, min_x=0, max_x=max_x, min_y=0., max_y=np.max(h))\n",
    "\n",
    "fluid_grid = StructuredGrid2D.rectangle_vertical_refine(\n",
    "    nelem_x, nelem_y_w, min_x=0, max_x=max_x, min_y=np.max(h),\n",
    "    max_y=np.max(wh) + np.max(h))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we add topography to both meshes using our predefined function. Note that we could do this also later after concatenating both meshes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make it an unstructured mesh\n",
    "solid_mesh = Skeleton([solid_grid]).get_unstructured_mesh() \n",
    "fluid_mesh = Skeleton([fluid_grid]).get_unstructured_mesh() \n",
    "\n",
    "# add topography to the mesh\n",
    "solid_mesh.add_dem_2D(x, h - np.max(h), y0=0., yref=np.max(h), y1=np.max(wh) + np.max(h))\n",
    "solid_mesh.apply_dem()\n",
    "\n",
    "fluid_mesh.add_dem_2D(x, h - np.max(h), y0=0., yref=np.max(h), y1=np.max(wh) + np.max(h))\n",
    "fluid_mesh.add_dem_2D(x, wh + h - np.max(h) - np.max(wh), y0=np.max(h),\n",
    "             yref=np.max(wh) + np.max(h), y1=np.inf)\n",
    "fluid_mesh.apply_dem()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at what we have to far. There are two meshes `solid_mesh` and `fluid_mesh` that include a sea bottom topography. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solid_mesh.plot(show=False)\n",
    "plt.show()\n",
    "\n",
    "fluid_mesh.apply_dem()\n",
    "fluid_mesh.plot(show=False)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it is time to attach the material properties to the mesh. In this example we use an `elemental` parameterization. This means that the model is defined on all vertices of an element and it might be discontinuous across element boundaries. In addition to velocities and density there is a field `fluid` that indicates whether an element is in the fluid or solid region of the domain. This is the only information about the coupled system that we need to provide. The interface between both media will be automatically detected in `salvus_wave`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Attach material parameters to the solid part of the mesh\n",
    "n_solid_elements = solid_mesh.nelem\n",
    "solid_mesh.attach_field('fluid', np.zeros(n_solid_elements))\n",
    "\n",
    "\n",
    "## get element centroids in case we want to modify the model\n",
    "# centroids = solid_mesh.get_element_centroid()\n",
    "\n",
    "for node in range(solid_mesh.nodes_per_element):\n",
    "    solid_mesh.attach_field(\"VP_{}\".format(node), np.ones(n_solid_elements) * vp_solid)    \n",
    "    solid_mesh.attach_field(\"VS_{}\".format(node), np.ones(n_solid_elements) * vs_solid)    \n",
    "    solid_mesh.attach_field(\"RHO_{}\".format(node), np.ones(n_solid_elements) * rho_solid)\n",
    "    \n",
    "# Attach material parameters to the fluid part of the mesh\n",
    "n_fluid_elements = fluid_mesh.nelem\n",
    "fluid_mesh.attach_field('fluid', np.ones(n_fluid_elements))\n",
    "for node in range(fluid_mesh.nodes_per_element):\n",
    "    fluid_mesh.attach_field(\"VP_{}\".format(node), np.ones(n_fluid_elements) * vp_fluid)\n",
    "    fluid_mesh.attach_field(\"VS_{}\".format(node), np.zeros(n_fluid_elements))    \n",
    "    fluid_mesh.attach_field(\"RHO_{}\".format(node), np.ones(n_fluid_elements) * rho_fluid)\n",
    "    \n",
    "#### use a region of interest to restrict the wavefield / gradient output \n",
    "# solid_mesh.attach_field('ROI', np.ones(n_solid_elements))    \n",
    "# fluid_mesh.attach_field('ROI', np.zeros(n_fluid_elements))    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Almost done. All that is left to do is adding both parts of the mesh together and flagging the `side sets`, i.e., the boundaries of the mesh. These can be used to define boundary conditions later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# concatenate fluid and solid part of the mesh\n",
    "mesh = solid_mesh + fluid_mesh\n",
    "\n",
    "# find side sets for boundary conditions\n",
    "mesh.find_side_sets('cartesian')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we write the mesh to an Exodus file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the mesh and write it to an exodus file\n",
    "mesh.plot(show=False)\n",
    "plt.show()\n",
    "mesh.write_exodus('coupled_media_with_topography.e')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's visualize the mesh in Paraview and inspect it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN ./coupled_media_with_topography.e "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What a sweet mesh!\n",
    "\n",
    "\n",
    "### Generating inputs\n",
    "\n",
    "Let's think about what we need to specify in order to run a wave simulation. Obviously, there are a mesh and a model, parameters to define the spatial and temporal discretization, sources and receivers and, optionally, further outputs.\n",
    "\n",
    "Note that Salvus will exit with an error message in case you forgot to specify a mandatory parameter. Always keep an eye on the output of the solver to make sure everything is working properly.\n",
    "\n",
    "Sources and receivers can be conveniently specified in a `toml` file. `toml` is a minimal configuration file format that is easy to read and to parse. A receiver needs at least to set the `network` and `station` name following the SEED format (http://www.fdsn.org/seed_manual/SEEDManual_V2.4.pdf). Furthermore, we need to specify whether the receiver is in fluid or solid media and its location, of course. \n",
    "\n",
    "Similarly to the receivers, we can define one or more sources in a `toml` file instead of passing all the options to the command line. \n",
    "Salvus can transform spherical coordinates into the internally used cartesian coordinates, but this currently only works for receivers and not for sources. Thus, the `toml` field for sources is called just `location` instead of `salvus_coordinates`.\n",
    "\n",
    "This is how the `toml` file can be created for a single source-receiver pair which are both in the fluid part of the domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile coupled_media_with_topography.toml\n",
    "\n",
    "[[source]]\n",
    "name = \"source1\"\n",
    "location = [2000.0, 3900.0]\n",
    "spatial_type = \"scalar\"\n",
    "temporal_type = \"ricker\"\n",
    "scale = [1e9]\n",
    "center_frequency = 3.0\n",
    "\n",
    "[[receiver]]\n",
    "network = \"RC\"\n",
    "station = \"000\"\n",
    "medium = \"fluid\"\n",
    "salvus_coordinates = [8000.0, 2500.0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Forward simulation\n",
    "\n",
    "Now, it is finally time for a wave simulation!\n",
    "\n",
    "In addition to the mesh and the toml file, we need to set a few parameters for the spatial and temporal discretization and to specify the boundary conditions.\n",
    "Note that we need to set the start time, the end time, and the time step explicitly to make sure we can use the same discretization for the adjoint run again. \n",
    "\n",
    "\n",
    "With volume output you can output a variety of different fields (displacement, velocities, ...) to an `hdf5` file and Salvus will automatically create an `xdmf` file for you to visualize the data with `Paraview`. \n",
    "\n",
    "Volume data can get very large so downsampling is fairly crucial here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -0.52\n",
    "    end-time-in-seconds = 7.0\n",
    "    time-step-in-seconds = 0.0014\n",
    "    source-toml-filename = \"coupled_media_with_topography.toml\"\n",
    "    \n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ELASTIC\", \"u_ACOUSTIC\"]\n",
    "    sampling-interval-in-time-steps = 50\n",
    "    filename = \"wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    \n",
    "[output.point-data]\n",
    "    fields = [\"u_ACOUSTIC\"]\n",
    "    sampling-interval-in-time-steps = 1\n",
    "    filename = \"receivers.h5\"\n",
    "    receiver-toml-filename = \"coupled_media_with_topography.toml\"\n",
    "    format = \"ASDF\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are you curious how the wavefield will look like? Let's take a look at it in `Paraview`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN wavefield_ACOUSTIC.xdmf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to make sure that the receiver has properly recorded the wavefield."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with pyasdf.ASDFDataSet(\"receivers.h5\") as ds:\n",
    "    print(ds.waveforms[\"RC_000\"])\n",
    "    ds.waveforms[\"RC_000\"].displacement[0].plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The adjoint method\n",
    "\n",
    "Let's recall the steps we need to follow to compute a sensitivity kernel:\n",
    "1. Run a forward simulation and store the wavefield\n",
    "2. Compute the adjoint source(s)\n",
    "3. Run an adjoint simulation and compute the kernel\n",
    "\n",
    "**Which fields do we need to store during the forward simulation?** \n",
    "\n",
    "It can be tedious to keep track of that and Salvus takes care of saving the right fields automatically if you tell it to save the `adjoint` fields."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -0.52\n",
    "    end-time-in-seconds = 7.0\n",
    "    time-step-in-seconds = 0.0014\n",
    "    source-toml-filename = \"coupled_media_with_topography.toml\"\n",
    "    \n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"adjoint\"]\n",
    "    sampling-interval-in-time-steps = 10\n",
    "    filename = \"forward_wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false\n",
    "    \n",
    "[output.point-data]\n",
    "    fields = [\"u_ACOUSTIC\"]\n",
    "    sampling-interval-in-time-steps = 1\n",
    "    filename = \"receivers.h5\"\n",
    "    receiver-toml-filename = \"coupled_media_with_topography.toml\"\n",
    "    format = \"ASDF\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great. We have synthetic receiver data now and stored the forward wavefield. The next step is to compute adjoint sources."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adjoint sources\n",
    "\n",
    "In the interest of time, we are not going to use observed data, but rather compute sensitivity kernels by reinjecting the time-reversed synthetic measurement itself. The sensitivity kernel that comes out of this adjoint source highlights all areas that have an impact (up to first order) on the received signal. The following python script allows you to compute the adjoint source for our problem. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_misfit_and_adjoint_sources(receiver_toml_file: pathlib.Path,\n",
    "                                       synthetic_asdf_file: pathlib.Path,\n",
    "                                       adjoint_source_asdf_file: pathlib.Path,\n",
    "                                       adjoint_source_toml_file: pathlib.Path,\n",
    "                                       observed_asdf_file: pathlib.Path=None,\n",
    "                                       waveform_tag=\"displacement\"):\n",
    "    \"\"\"\n",
    "    Computes the misfit and adjoint sources for the given ASDF file containing\n",
    "    synthetic data.\n",
    "\n",
    "    :param synthetic_asdf_file: The ASDF file with the synthetic data.\n",
    "    :param adjoint_source_file: The output ASDF file with the adjoint\n",
    "        source. Must not exist yet.\n",
    "    :param adjoint_source_toml_file: The accompanying source TOML file so\n",
    "        Salvus can actually use the adjoint source in the ASDF file. Must not\n",
    "        exist yet.\n",
    "    \"\"\"\n",
    "\n",
    "    assert receiver_toml_file.exists(), receiver_toml_file\n",
    "    assert synthetic_asdf_file.exists(), synthetic_asdf_file  \n",
    "    \n",
    "    if adjoint_source_asdf_file.exists():\n",
    "        os.remove(adjoint_source_asdf_file)\n",
    "    if adjoint_source_toml_file.exists():\n",
    "        os.remove(adjoint_source_toml_file)\n",
    "    \n",
    "    if observed_asdf_file:\n",
    "        assert observed_asdf_file.exists(), observed_asdf_file\n",
    "        ds_obs = pyasdf.ASDFDataSet(str(observed_asdf_file))\n",
    "\n",
    "    misfit = 0.0\n",
    "    sources = []\n",
    "\n",
    "    recToml = toml.load(str(receiver_toml_file))\n",
    "                  \n",
    "    with pyasdf.ASDFDataSet(str(synthetic_asdf_file)) as ds_in, \\\n",
    "         pyasdf.ASDFDataSet(str(adjoint_source_asdf_file)) as ds_out:\n",
    "\n",
    "        for station in ds_in.waveforms:\n",
    "            station_name = station._station_name.replace(\".\", \"_\")\n",
    "\n",
    "            for rec in recToml[\"receiver\"]:\n",
    "                rec_name = rec.get(\"network\") + \"_\" + rec.get(\"station\")\n",
    "\n",
    "                if (rec_name == station_name):\n",
    "                    loc = rec.get(\"salvus_coordinates\")\n",
    "                    print(\"loc:\", loc, \"rec_name:\", rec_name, \"station_name:\",station_name)\n",
    "\n",
    "            tr = station.displacement[0]\n",
    "            adj_src = np.empty((tr.stats.npts))\n",
    "\n",
    "            if observed_asdf_file:\n",
    "                tr_obs = ds_obs.waveforms[station_name].displacement[0]\n",
    "                tr_obs.interpolate(tr.stats.sampling_rate)\n",
    "                misfit += tr.stats.delta * ((tr.data - tr_obs.data) ** 2).sum()\n",
    "                adj_src = -tr.stats.delta * (tr.data[::-1] - tr_obs.data[::-1])\n",
    "                \n",
    "            else:\n",
    "                misfit += tr.stats.delta * (tr.data ** 2).sum()\n",
    "                adj_src = -tr.stats.delta * tr.data[::-1]\n",
    "                \n",
    "            ds_out.add_auxiliary_data(\n",
    "                   data=adj_src,\n",
    "                   data_type=\"AdjointSources\",\n",
    "                   path=station_name,\n",
    "                   parameters={\n",
    "                       \"dt\": tr.stats.delta,\n",
    "                       \"starttime\": 1e9*float(tr.stats.starttime),\n",
    "                        \"location\": loc,\n",
    "                       \"spatial-type\": np.string_(b\"scalar\\x00\")})\n",
    "\n",
    "            ds_name = \"/AuxiliaryData/AdjointSources/\" + station_name\n",
    "            sources.append({\"name\": station_name,\n",
    "                            \"dataset_name\": ds_name})                \n",
    "\n",
    "\n",
    "    with open(str(adjoint_source_toml_file), \"wt\") as fh:\n",
    "        toml.dump({\"source_input_file\": str(adjoint_source_asdf_file.absolute()),\n",
    "                   \"source\": sources}, fh)\n",
    "\n",
    "\n",
    "    return (0.5*misfit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that it is possible to pass another asdf file containing observed data to that function. As mentioned before we will skip this here, but feel free to play around with different models and to generate your own \"observed\" data.\n",
    "\n",
    "Execute the following cell to time reverse the recorded signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "receiver_toml_file = pathlib.Path('coupled_media_with_topography.toml')\n",
    "synthetic_asdf_file = pathlib.Path('receivers.h5')\n",
    "# observed_asdf_file = pathlib.Path('observed.h5')\n",
    "adjoint_source_asdf_file = pathlib.Path('adjointSource.h5')\n",
    "adjoint_source_toml_file = pathlib.Path('adjointSource.toml')\n",
    "\n",
    "misfit = compute_misfit_and_adjoint_sources(receiver_toml_file, synthetic_asdf_file,\n",
    "                                            adjoint_source_asdf_file, adjoint_source_toml_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is now an `hdf5` file that contains the adjoint sources and a `toml` file that we can use as input for the adjoint simulation. The `toml` file specifies the `source_input_file` where the adjoint traces are stored as well as the location (groups and datasets) in this file. Location, spatial type and time are stored directly in the `hdf5` file. Take a look at the adjoint sources. Plot them with `pyasdf`, but remember that they are not stored as `Waveforms`, but in the group `AuxiliaryData`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat adjointSource.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with pyasdf.ASDFDataSet(\"adjointSource.h5\") as ds_adjSrc:\n",
    "    print(ds_adjSrc.auxiliary_data[\"AdjointSources\"])\n",
    "    plt.plot(ds_adjSrc.auxiliary_data.AdjointSources[\"RC_000\"].data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adjoint simulation\n",
    "\n",
    "Ok, almost there. Now we only need to run the adjoint simulation. First we need to tell Salvus, that we want to run a simulation in adjoint mode by adding the `[adjoint]` group. Next, we specify the adjoint source with the `toml` file we created. And finally, we need to tell Salvus to load the forward wavefield and the name of the file to which it should write the kernel using the following options:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -0.52\n",
    "    end-time-in-seconds = 7.0\n",
    "    time-step-in-seconds = 0.0014\n",
    "    source-toml-filename = \"adjointSource.toml\"\n",
    "    \n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[adjoint]\n",
    "    gradient-filename = \"kernel.e\"\n",
    "    forward-wavefield-filename = \"forward_wavefield.h5\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN kernel.e"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The shape of the kernel looks reasonable and follows roughly the raypath along which the waves travel from the source to the receiver location."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -0.52\n",
    "    end-time-in-seconds = 7.0\n",
    "    time-step-in-seconds = 0.0014\n",
    "    source-toml-filename = \"adjointSource.toml\"\n",
    "    \n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[adjoint]\n",
    "    gradient-filename = \"kernel.e\"\n",
    "    forward-wavefield-filename = \"forward_wavefield.h5\"\n",
    "    gradient-fields = [\"VP\", \"VS\", \"RHO\"]\n",
    "    \n",
    "[output.volume-data]\n",
    "    fields = [\"u_ACOUSTIC\", \"u_ELASTIC\", \"gradient\"]\n",
    "    sampling-interval-in-time-steps = 30\n",
    "    filename = \"adjoint_wavefield.h5\"\n",
    "    polynomial-order = 4\n",
    "    format = \"HDF5\"\n",
    "    region-of-interest = false"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN adjoint_wavefield_ACOUSTIC.xdmf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The kernel lives in the same vector space as the model so it is stored in the same format. By default, Salvus computes the derivatives with respect to the parameters that enter linearly into the wave equation. But maybe you are interested in the $v_p$- and $v_s$-kernel instead? No problem, we can do that, too. Just use the `gradient-fields` option to compute the kernels using another parameterization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile input.toml\n",
    "\n",
    "[domain]\n",
    "    dimension = 2\n",
    "    polynomial-order = 4\n",
    "    \n",
    "    [domain.mesh]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "    \n",
    "    [domain.model]\n",
    "        filename = \"coupled_media_with_topography.e\"\n",
    "        format = \"exodus\"\n",
    "        \n",
    "[physics.wave-equation]\n",
    "    time-stepping-scheme = \"newmark\"\n",
    "    start-time-in-seconds = -0.52\n",
    "    end-time-in-seconds = 7.0\n",
    "    time-step-in-seconds = 0.0014\n",
    "    source-toml-filename = \"adjointSource.toml\"\n",
    "    \n",
    "    [[physics.wave-equation.boundaries]]\n",
    "        type = \"absorbing\"\n",
    "        side-sets = [\"x0\",\"x1\",\"y0\"]\n",
    "    \n",
    "[adjoint]\n",
    "    gradient-filename = \"kernel.e\"\n",
    "    forward-wavefield-filename = \"forward_wavefield.h5\"\n",
    "    gradient-fields = [\"VP\", \"VS\", \"RHO\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!salvus input.toml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!$PARAVIEW_BIN kernel.e"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Feel free to play around with setup and try out how the kernel changes in different settings. For instance, you could:\n",
    "\n",
    "* change the location of source and receiver,\n",
    "* pick windows when computing the adjoint source,\n",
    "* add more sources and/or receivers,\n",
    "* change the material model, i.e., by adding a velocity gradient based on the centroids of the elements.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Fin.**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
